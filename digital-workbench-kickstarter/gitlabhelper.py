#!/usr/bin/python -u

import sys
import os
import logging
from argparse import ArgumentParser
from multiprocessing import Process

gatewayName=''
serviceName=''
BASEDIR=''

EXECUTION_LOG_PATH = '/tmp/log-kickstart.log'
LOG_TEXT_FORMAT = '%(asctime)s %(levelname)s:%(message)s'
MAVEN_COMMAND = './mvnw package -Pprod doeckerfile:build'
JHIPSTER_COMMAND = 'jhipster'
JHIPSTER_REGD_COMMAND = 'java -jar ./jhipster-registry-4.0.0.war &'
ouputRedirection = ' >> '+ EXECUTION_LOG_PATH + ' 2>&1'
#JDL_FILEPATH = BASEDIR + os.sep + 'bank-jdl-jhipster.jh'

def printLogData(logMessage, logLevel=1 ):
    print ('    ' + logMessage + '\n' )
    #if not logLevel:
    #    logLevel=1
    if ( logLevel == 1):
        logging.info(logMessage)
    elif ( logLevel == 2):
        logging.debug(logMessage)
    else :
        logging.error(logMessage)
 

def takePause(timeSec):
    os.system ('sleep ' + str(timeSec))

# Create badic folder structure
def createPrerequisiteStruct ():
    os.chdir ('/Users/atanughosh/Documents/starter')
    printLogData ("Creating folder structure for Gateway, " + gatewayName + " and Service Microservices, " + serviceName , 1 )
    os.makedirs(BASEDIR)
    os.makedirs (GATEWAYPATH)
    os.makedirs (SERVICEPATH)
    os.system("cp ./jhipster-registry-4.0.0.war " + BASEDIR + os.sep)
    os.system("cp ./*.json " + BASEDIR + os.sep)
    os.system("cp ./*.jh " + BASEDIR + os.sep)

def configureGateway ():
    printLogData ("Configuring the Gateway Application Structure...", 1)
    os.chdir(GATEWAYPATH)
    #os.system("cp c../yo-gw.json  ./.yo-rc.json")
    os.system ("sed -e 's/<GATEWAYSERVICE>/" + gatewayName + "/g' " + BASEDIR + "/yo-gw.json > ./.yo-rc.json")
    os.system (JHIPSTER_COMMAND  + ouputRedirection)
    os.chdir (BASEDIR)

    importJdlEntities (GATEWAYPATH)

def configureServiceInstance ():
    printLogData ("Configuring the Service Application Instance Structure...", 1)
    os.makedirs (SERVICEPATH)
    os.chdir(SERVICEPATH)
    #os.system("cp ../yo-serv.json  ./.yo-rc.json")
    os.system ("sed -e 's/<SERVICEINSTANCE>/" + serviceName + "/g' " + BASEDIR + "/yo-serv.json > ./.yo-rc.json")
    os.system (JHIPSTER_COMMAND + ouputRedirection)
    os.chdir (BASEDIR)

    importJdlEntities (SERVICEPATH)

def startJhipsterRegistry ():
    printLogData ("Started Jhipster Registry in the background from location: " + BASEDIR, 1)
    os.chdir (BASEDIR)
    os.system( JHIPSTER_REGD_COMMAND + ouputRedirection)
    takePause(20)

def startMavenCompilation (dirName):
    printLogData ("Started parallel Maven Compilation for " + dirName, 1)
    os.chdir(dirName)
    os.system(MAVEN_COMMAND + ouputRedirection)
    os.chdir (BASEDIR)
    takePause(60)

def importJdlEntities (dirName) :
    printLogData ("Importing the defined entities in " + dirName, 1)
    os.chdir (dirName)
    os.system (JDL_IMPORT_COMMAND + ouputRedirection) 

def runInParallel(*fns):
  proc = []
  for fn in fns:
    p = Process(target=fn)
    p.start()
    proc.append(p)
  for p in proc:
    p.join()

def createDockerImages (imageName):
    printLogData("Creating Docker Image for: " + imageName, 1)
    os.system("docker image tag " + imageName + " atanughosh/" + imageName)
    os.system("docker push atanughosh/" + imageName)

def deployToGoogleCloud (imageName):
    printLogData("About to deploy Image for: " + imageName, 1)
    os.chdir(BASEDIR)
    os.system("kubectl apply -f " + imageName)

def printExternalUrlInformation ():
    printLogData("Services are deployed to Production. About to generate External Access URL")

########
# MAIN #
########
logging.basicConfig(filename=EXECUTION_LOG_PATH, format=LOG_TEXT_FORMAT, level=logging.DEBUG)
parser = ArgumentParser()
parser.add_argument("-o", "--operation",
                    help="Operation to Perform")
parser.add_argument("-u", "--username",
                    help="Gitlab Username")
parser.add_argument("-t", "--accesstoken",
                    help="Gitlab Admin Token")
parser.add_argument("-b", "--basedir",
                    help="Base Folder Path Local")
parser.add_argument("-p", "--projectname",
                    help="Gitlab Project Name")

arguments = parser.parse_args()


printLogData ("###### Started Execution ###### ", 2)

operationName=arguments.operation
gitlabusername=arguments.username
gitlabadmintoken=arguments.accesstoken
gitlabprojectname=arguments.projectname
BASEDIR=arguments.basedir

printLogData ("Operation: " + str(operationName) + ", User: " + str(gitlabusername) + ", Token: " + str(gitlabadmintoken) + ", Project: " + str(gitlabprojectname)+ ", Basedir: " +str(BASEDIR), 2)

JDL_FILEPATH=arguments.jdlfilepath
JDL_IMPORT_COMMAND = 'jhipster import-jdl ' + JDL_FILEPATH + ' --force'

printLogData ("Operatrion: " + operationName + ", Gateway: " + gatewayName + ", Service: " + serviceName + ", BASEDIR: " + BASEDIR, 2)

if os.path.exists(BASEDIR):
    printLogData ("Base Directory, " + BASEDIR + " already present, skipping folder application configuration", 1)
if operationName == 'BootstrapBase':
    createPrerequisiteStruct ()

    # startJhipsterRegistry ()
elif operationName == 'BootstrapGateway':

    configureGateway ()
elif operationName == 'BootstrapService':
    configureServiceInstance ()

#elif operationName == 'ImportJdl':
#    JDL_FILEPATH=arguments.jdlfilepath
#    JDL_IMPORT_COMMAND = 'jhipster import-jdl ' + JDL_FILEPATH + ' --force'

#    importJdlEntities (GATEWAYPATH)
#    importJdlEntities (SERVICEPATH)
else:
    printLogData ("Invalid Option : " + operationName)


    printLogData ("Do Nothing... : " + operationName)
    #startMavenCompilation (GATEWAYPATH)
    #startMavenCompilation (SERVICEPATH)

    #createDockerImages (gatewayName)
    #createDockerImages (serviceName)


    #deployToGoogleCloud ('registry')
    #deployToGoogleCloud (gatewayName)
    #deployToGoogleCloud (serviceName)
    #takePause (10)

    #printExternalUrlInformation ()


printLogData ("###### Completed Execution ###### ", 2)
