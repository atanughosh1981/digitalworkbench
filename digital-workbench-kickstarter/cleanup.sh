#!/bin/sh
WORKSPACE_BASE="/Users/atanughosh/Documents/demoauto/projectacme/"
CURRENT_DIR=`pwd`
echo ""
echo ""
echo "Deleting Generated Project from repo"
cd ${WORKSPACE_BASE}/acmebank/;git stash; git pull origin master; rm -rf ../acmebank/*; git add .; git commit -m “cleaned”; git push origin master -f; cd - >> /dev/null
cd ${WORKSPACE_BASE}/acmeaccount/;git stash; git pull origin master; rm -rf ../acmeaccount/*; git add .; git commit -m “cleaned”; git push origin master -f; cd - >> /dev/null
cd ${WORKSPACE_BASE}/acmetransaction/;git stash; git pull origin master; rm -rf ../acmetransaction/*; git add .; git commit -m “cleaned”; git push origin master -f; cd - >> /dev/null

cd ${CURRENT_DIR}
echo ""
echo ""
echo "Removing cloud deployment..."

/usr/local/bin/kubectl delete deployment --all
echo ""
echo ""
